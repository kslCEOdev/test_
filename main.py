import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html

import flask
import pandas as pd
import time
import os

from class_person import Person

person = Person()

server = flask.Flask('app')
server.secret_key = os.environ.get('secret_key', 'secret')

app = dash.Dash('app', server=server)

app.scripts.config.serve_locally = False
dcc._js_dist[0]['external_url'] = 'https://cdn.plot.ly/plotly-basic-latest.min.js'

ALLOWED_FIELDS = ['Фамилия', 'Имя', 'Отчество']

app.layout = html.Div([
    html.H1('Карточка физического лица'),
    dcc.Input(id='person-first-name',
            type='text',
            placeholder=person.first_name,
    ),
        dcc.Input(id='person-middle-name',
            type='text',
            placeholder=person.middle_name,
    ),

    dcc.Input(id='person-last-name',
            type='text',
            placeholder=person.last_name,
    ),

    html.Div([html.Button('Сохранить', id = 'save-person'),
            html.Div(id='status')])
    
   
], className="container")

@app.callback(
    Output('status', 'children'),
    [Input('save-person', 'n_clicks')],
    [State('person-first-name', 'value'),
    State('person-middle-name', 'value'),
    State('person-last-name', 'value')]
)
def save_(n_clicks, personfirstname, personmiddlename, personlastname):
    res = person.update_current_records(personfirstname, personmiddlename, personlastname)
    return res


if __name__ == '__main__':
    app.run_server()